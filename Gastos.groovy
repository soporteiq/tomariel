def issueId = issue.key
//Capturos los datos iniciales de la facturación(Cuenta, fecha inicio y fecha fin)
String datos= FacturacionDatos(issueId)
def separarDatos = datos.split("_");
def cuenta_id=separarDatos[0]
def cuenta=separarDatos[1]
def FechaInicio=separarDatos[2]
def FechaFin=separarDatos[3]
double Extras= GastoExtras(FechaInicio,FechaFin,cuenta)
//String MensualUsuario= PresupuestoMensual(cuenta_id)
def MensualUsuario=PresupuestoMensualNewApi(cuenta_id)
def SepararDatosCuenta = MensualUsuario.split("_");
def ClaveCuenta=SepararDatosCuenta[1]
//imputaciones
def tiempoRegistrado=WorkLogTempoNewApi(FechaInicio,FechaFin,ClaveCuenta)
double totalHorasImputadas=0
for(int i=0; i<tiempoRegistrado.size();i++){
   
   //if(ClaveCuenta==tiempoRegistrado[i].issue.accountKey){
       
    def presupuestoParticular=CuentasDataPresupuestoNewApi(tiempoRegistrado[i].author.accountId)
    int PreEntero= Integer.parseInt(presupuestoParticular);
    //saco el presupuesto mensual
    BigDecimal presuMensualCalculo= (PreEntero/12)
    double PresupuestoMensualFinal=presuMensualCalculo.round(2)
    //def HoraSeparado = tiempoRegistrado[i].timeSpent.split("h");
    double HoraEntero= (tiempoRegistrado[i].timeSpentSeconds/3600)
    totalHorasImputadas+= (PresupuestoMensualFinal*HoraEntero)
//}
   
}
//actualizo el gasto
ActualizarGastosTotales(Extras,issueId,totalHorasImputadas)

public String FacturacionDatos(String key){
    
    def consultaCuenta= get('https://tomarial.atlassian.net/rest/api/3/issue/'+key)
    .header("Content-Type", "application/json")
    .asObject(Map).body
    
    return consultaCuenta.fields.customfield_10029.id+"_"+consultaCuenta.fields.customfield_10029.value+"_"+consultaCuenta.fields.customfield_10047+"_"+consultaCuenta.fields.customfield_10048
}

public String PresupuestoMensual(String cuenta){
    def consultaCuenta= get('https://app.tempo.io/rest/tempo-accounts/2/accounts/'+cuenta)
    .header("Authorization", "Bearer 2UU024DKB06GCRZK5gscioVvK0BMrt")
    .header("Content-Type", "application/json").asObject(Map)
    def cuentas = consultaCuenta.body
    return cuentas.monthlyBudget.toString()+"_"+cuentas.key
}
public String PresupuestoMensualNewApi(String cuenta){
    def consultaCuenta= get('https://api.tempo.io/core/3/accounts/')
    .header("Authorization", "Bearer 2UU024DKB06GCRZK5gscioVvK0BMrt")
    .header("Content-Type", "application/json").asObject(Map)
    def cuentas = consultaCuenta.body
    
    for(int i=0;i<cuentas.results.size();i++){
        
        if(cuentas.results[i].id.toString()==cuenta){
            return cuentas.results[i].monthlyBudget+"_"+cuentas.results[i].key
        }
    }
    //return cuentas.monthlyBudget.toString()+"_"+cuentas.key
}
public double GastoExtras(String ini, String fin, String cuenta){
    
    def consultaCuenta= get('https://tomarial.atlassian.net/rest/api/3/search')
    .header("Content-Type", "application/json")
    .queryString("jql", '"Fecha del gasto[Date]" >= '+ini+' AND "Fecha del gasto[Date]" <= '+fin+' AND project = FAC AND issuetype = Gastos AND Account = "'+cuenta+'"')
    .asObject(Map).body
    
    double total=0
    for(int i=0;i<consultaCuenta.issues.size();i++){
        
       total+=consultaCuenta.issues[i].fields.customfield_10044
    }
    return total
}
public String ActualizarGastosTotales(double gastosExtra, String key, double totalHoras){
    
    //double HporPresupuesta= totalHoras
    double total= totalHoras+gastosExtra
    
    
    //actualizo el valor del campo ingreso total
    def result = put("https://tomarial.atlassian.net/rest/api/2/issue/"+key)
            .header('Content-Type', 'application/json')
            .body([
                    fields: [
                            customfield_10056: total
                    ]
            ])
            .asString()
            
    return result
}
public String AccountId(String account){
    def consultaCuenta= get('https://app.tempo.io/rest/tempo-accounts/2/accounts/'+account)
    .header("Authorization", "Bearer 2UU024DKB06GCRZK5gscioVvK0BMrt")
    .header("Content-Type", "application/json").asObject(Map)
    def cuenta = consultaCuenta.body
    return cuenta.lead.accountId
}
public List WorkLogTempo(String ini, String fin){

    def consultaCuenta= post('https://app.tempo.io/rest/tempo-timesheets/4/worklogs/search')
    .header("Authorization", "Bearer 2UU024DKB06GCRZK5gscioVvK0BMrt")
    .header("Content-Type", "application/json")
    .body([from:ini,to: fin]).asObject(List).body
    
    return consultaCuenta
}
public List WorkLogTempoNewApi(String ini, String fin, String cuenta){

    def consultaCuenta= get('https://api.tempo.io/core/3/worklogs/account/'+cuenta+'?from='+ini+'&to='+fin)
    .header("Authorization", "Bearer 2UU024DKB06GCRZK5gscioVvK0BMrt")
    .header("Content-Type", "application/json").asObject(Map).body
    
    return consultaCuenta.results
}

public String CuentasDataPresupuesto(String clave){

    def consultaCuenta= get('https://app.tempo.io/rest/tempo-accounts/2/accounts/?expand=true&skipArchived=true')
    .header("Authorization", "Bearer 2UU024DKB06GCRZK5gscioVvK0BMrt")
    .header("Content-Type", "application/json")
    .asObject(List).body
    
    for(int i=0;i<consultaCuenta.size();i++){
        
        if(consultaCuenta[i].category.id==7 && consultaCuenta[i].lead.accountId==clave){
            return consultaCuenta[i].monthlyBudget
        }
    }
}
public String CuentasDataPresupuestoNewApi(String cuenta){
    def consultaCuenta= get('https://api.tempo.io/core/3/accounts/')
    .header("Authorization", "Bearer 2UU024DKB06GCRZK5gscioVvK0BMrt")
    .header("Content-Type", "application/json").asObject(Map)
    def cuentas = consultaCuenta.body
    
    for(int i=0;i<cuentas.results.size();i++){
        
        if(cuentas.results[i].category.id==7 && cuentas.results[i].lead.accountId.toString()==cuenta){
            return cuentas.results[i].monthlyBudget
        }
        
    }
    
}