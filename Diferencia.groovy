def issueId = issue.key
String datos= FacturacionDatos(issueId)
def separarDatos = datos.split("_");
def Ingresos=separarDatos[0]
def Gastos=separarDatos[1] 


ActualizarDiferencia(Ingresos,Gastos,issueId)
public String FacturacionDatos(String key){
    
    def consultaCuenta= get('https://tomarial.atlassian.net/rest/api/3/issue/'+key)
    .header("Content-Type", "application/json")
    .asObject(Map).body
    
    def ingreso
    if(consultaCuenta.fields.customfield_10055!=null){
        ingreso= consultaCuenta.fields.customfield_10055
    }else{
        ingreso=0
    }
    def gasto
    if(consultaCuenta.fields.customfield_10056!=null){
        gasto= consultaCuenta.fields.customfield_10056
    }else{
        gasto=0
    }
    
    return ingreso+"_"+gasto
}


public String ActualizarDiferencia(String Ingreso, String Gasto, String key){
    
    double IN = Double.parseDouble(Ingreso);
    double GA = Double.parseDouble(Gasto);
    double total= (IN-GA)
    
    
    //actualizo el valor del campo ingreso total
    def result = put("https://tomarial.atlassian.net/rest/api/2/issue/"+key)
            .header('Content-Type', 'application/json')
            .body([
                    fields: [
                            customfield_10051: total
                    ]
            ])
            .asString()
            
    return result
}