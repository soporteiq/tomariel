def issueKey = issue.key
String datos= FacturacionDatos(issueKey)
def separarDatos = datos.split("_");
def cuenta_id=separarDatos[0]
def cuenta=separarDatos[1]
def FechaInicio=separarDatos[2]
def FechaFin=separarDatos[3]
double Extras= IngresosExtras(FechaInicio,FechaFin,cuenta)
String MensualUsuario= PresupuestoMensual(cuenta_id)
ActualizarIngresosTotales(MensualUsuario,Extras,issueKey)
public String FacturacionDatos(String key){
    
    def consultaCuenta= get('https://tomarial.atlassian.net/rest/api/3/issue/'+key)
    .header("Content-Type", "application/json")
    .asObject(Map).body
    
    return consultaCuenta.fields.customfield_10029.id+"_"+consultaCuenta.fields.customfield_10029.value+"_"+consultaCuenta.fields.customfield_10047+"_"+consultaCuenta.fields.customfield_10048
}

public String PresupuestoMensual(String cuenta){
    def consultaCuenta= get('https://app.tempo.io/rest/tempo-accounts/2/accounts/'+cuenta)
    .header("Authorization", "Bearer 2UU024DKB06GCRZK5gscioVvK0BMrt")
    .header("Content-Type", "application/json").asObject(Map)
    def cuentas = consultaCuenta.body
    return cuentas.monthlyBudget.toString()
}
public double IngresosExtras(String ini, String fin, String cuenta){
    
    def consultaCuenta= get('https://tomarial.atlassian.net/rest/api/3/search')
    .header("Content-Type", "application/json")
    .queryString("jql", '"Fecha Ingreso[Date]" >= '+ini+' AND "Fecha Ingreso[Date]" <= '+fin+' AND project = FAC AND issuetype = Ingresos AND Account = "'+cuenta+'"')
    .asObject(Map).body
    
    double total=0
    for(int i=0;i<consultaCuenta.issues.size();i++){
        
       total+=consultaCuenta.issues[i].fields.customfield_10038
    }
    return total
}
public String ActualizarIngresosTotales(String presupuesto, double ingresosExtra, key){
    
    double Presu = Double.parseDouble(presupuesto);
    //saco el presupuesto mensual
    BigDecimal presuMensualCalculo= (Presu/12)
    double PresupuestoMensualFinal=presuMensualCalculo.round(2)
    double total= PresupuestoMensualFinal+ingresosExtra
    
    
    //actualizo el valor del campo ingreso total
    def result = put("https://tomarial.atlassian.net/rest/api/2/issue/"+key)
            .header('Content-Type', 'application/json')
            .body([
                    fields: [
                            customfield_10055: total
                    ]
            ])
            .asString()
            
    return result
}

